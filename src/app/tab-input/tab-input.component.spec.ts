import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TabInputComponent } from './tab-input.component';

describe('TabInputComponent', () => {
  let component: TabInputComponent;
  let fixture: ComponentFixture<TabInputComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TabInputComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TabInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
