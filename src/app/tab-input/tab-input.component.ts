import { Component, OnInit, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-tab-input',
  templateUrl: './tab-input.component.html',
  styleUrls: ['./tab-input.component.css']
})
export class TabInputComponent implements OnInit {
inputDate: Date;
inputLoc: String;
inputHome: String;
inputGuest: String;
inputUrlHome: URL;
inputUrlGuest: URL;
inputDesc: String;

@Output() matchDataCreated = new EventEmitter<{inputDate: Date, inputLoc: String, inputHome: String, 
  inputGuest: String, inputUrlHome: URL, inputUrlGuest: URL, inputDesc: String}>()

onAddMatchData() {
  this.matchDataCreated.emit({
    inputDate: this.inputDate,
    inputLoc: this.inputLoc,
    inputHome: this.inputHome,
    inputGuest: this.inputGuest,
    inputUrlHome: this.inputUrlHome,
    inputUrlGuest: this.inputUrlGuest,
    inputDesc: this.inputDesc
  })
}

  constructor() { }

  ngOnInit() {
  }

}
