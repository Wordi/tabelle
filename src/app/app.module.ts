import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { TabViewComponent } from './tab-view/tab-view.component';
import { TabInputComponent } from './tab-input/tab-input.component';
import { Anouncement } from './match/match.model';

@NgModule({
  declarations: [
    AppComponent,
    TabViewComponent,
    TabInputComponent
    
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
