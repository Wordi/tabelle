import { Component } from '@angular/core';
import { Anouncement } from './match/match.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app works!';

  matchPreview: Anouncement[] = [
    new Anouncement(new Date(2017, 6, 25), 'Herrenwaldstadion', 'Sportfreunde Blau-Gelb Marburg', 'Eintracht Stadtallendorf',
    new URL ('http://blaugelb-jugend.de/wp-content/uploads/2010/02/logo_neu.jpg'), new URL ('https://upload.wikimedia.org'+
    '/wikipedia/de/6/68/Logo_tsv_eintracht_stadtallendorf.gif'), 'Wir werden wahrscheinlich ein sehr'+
    'spannendes Fussballspiel zu sehen bekommen. Beide Mannschaften sind top-fit und bis in die Haarspitzen motiviert')
  ];

  onMatchDataAdded(matchData: {inputDate: Date, inputLoc: String, inputHome: String, 
    inputGuest: String, inputUrlHome: URL, inputUrlGuest: URL, inputDesc: String}) {
      this.matchPreview.push({
        date: new Date(matchData.inputDate),
        loc: matchData.inputLoc,
        homeTeam: matchData.inputHome,
        guestTeam: matchData.inputGuest,
        homeEm: new URL('matchData.inputUrlHome'),
        guestEm: new URL('matchData.inputUrlGuest'),
        desc: matchData.inputDesc
      });
  }
}
