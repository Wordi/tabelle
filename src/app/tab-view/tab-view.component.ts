import { Component, OnInit, Input } from '@angular/core';



@Component({
  selector: 'app-tab-view',
  templateUrl: './tab-view.component.html',
  styleUrls: ['./tab-view.component.css']
})

export class TabViewComponent implements OnInit {
@Input() previews: {date: Date, loc: String, homeTeam: String, guestTeam: String, homeEm: URL, guestEm: URL, desc: String}[];


  constructor() { }

  ngOnInit() {
  }

}
