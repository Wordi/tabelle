export class Anouncement {



/*
constructor(public date: Date, public loc: String,
 public homeTeam: String, public guestTeam: String,
  public homeEm: URL, public guestEm: URL, public desc: String) {}
*/

 
public date: Date;
public loc: String;
public homeTeam: String;
public guestTeam: String;
public homeEm: URL;
public guestEm: URL;
public desc: String;

constructor(date: Date, loc: String, homeTeam: String, guestTeam: String, homeEm: URL, 
 guestEm: URL, desc: String) {
this.date = date;
this.loc = loc;
this.homeTeam = homeTeam;
this.guestTeam = guestTeam;
this.homeEm = homeEm;
this.guestEm = guestEm;
this.desc = desc;
 }
}