import { TopSpielPage } from './app.po';

describe('top-spiel App', function() {
  let page: TopSpielPage;

  beforeEach(() => {
    page = new TopSpielPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
